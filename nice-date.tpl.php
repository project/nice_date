<?php
/**
 * @file
 *
 * Provides the layout for the Nice Date element.
 */
?>
<div class="nice_date">
  <div class="<?php print $month_attributes; ?>"><?php print $month_long; ?></div>
  <div class="<?php print $day_attributes;   ?>"><?php print $day_long;   ?></div>
  <div class="<?php print $year_attributes;  ?>"><?php print $year_long;  ?></div>
</div>
